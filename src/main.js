// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import { Container, Header, Aside, Main, Button, Input, Notification, Avatar } from 'element-ui';

import App from './App'
import router from './router'


import 'element-ui/lib/theme-chalk/index.css';
Vue.use(Container)
Vue.use(Header)
Vue.use(Aside)
Vue.use(Main)
Vue.use(Button)
Vue.use(Input)
Vue.use(Avatar)
Vue.prototype.$notify = Notification;

import vuescroll from 'vuescroll/dist/vuescroll-native';
Vue.use(vuescroll);

import 'viewerjs/dist/viewer.css'
import Viewer from 'v-viewer'
Vue.use(Viewer)

import './assets/css/base.css'
import './assets/font/iconfont.css'

import store from './store/index'


Vue.config.productionTip = false

Vue.prototype.globalClick = function(callback) {
    document.getElementById('app').onclick = function() {
        callback()
    };
};

new Vue({
    el: '#app',
    router,
    store,
    components: { App },
    template: '<App/>'
})
