import Vue from 'vue'
import Router from 'vue-router'
// import home from '@/components/home'
// import user from '@/components/user'
// import customer from '@/components/customer'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/home'
    },
    {
      path: '/home',
      name: 'home',
      component: ()=> import('@/components/home'),
      children: [
        {
          path: '',
          component: ()=> import('@/components/user')
        },
        {
          path: '/customer',
          component: ()=> import('@/components/customer')
        }
      ]
    }
  ],
  linkActiveClass: 'active'
})
